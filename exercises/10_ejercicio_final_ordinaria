Una organización está organizada en dos departamentos:
- deptA
- deptB

Cada departamento ha de estar aislado del otro y ha de poder acceder
a una red de servidores que llamaremos "servers". También se dipone de 
una red wifi que no ha de poder acceder a las redes internas, y que sólo debe dejar
acceder al puerto 80 y 443 del servidor web de la red de servers. Desde
la red wifi también se podrá navegar a internet usando los puertos destino 80 y 443, pero
no se podrán establecer conexiones telnet, ssh o ftp.

Se ha contratado una línea empresarial a un ISP, que ha de ser utilizada
sólo por los servidores, excepto en caso de caída del "router inf" que
es el que se usa habitulamente como router de salida de las redes de 
los departamentos y de los dispositivos conectados por wifi.

Se ha de priorizar el tráfico del deptA respecto al deptB y se ha de 
repartir el ancho de banda por igual entre los usuarios de cada departamento.

Se ha de limitar el tráfico de bajada de la wifi a 500Kbps

El servidor ha de ser accesible desde inernet usando el puerto externo 77XX

Se ha de limitar el acceso por ssh (cambiando el puerto al 12322) al 
router desde cualquier red, y sólo se ha de poder hacer telnet desde la red de control

Se disone de un mikrotik de 4 puertos y de 6 puertos del switch para cada
puesto de trabajo que se conectarán de la siguiente forma:


MIKROTIK                   tagged               untagged   destino 
--------------------------------------------------------------------------
puerto 1   internet        escola,isp                      sw-puerto4 
puerto 2   puerto usb pc                                   pc-usb-ethernet 
puerto 3   redes internas  deptA,deptB,servers             sw-puerto5 
puerto 4      

     
SWITCH                   tagged          untagged type destino
-------------------------------------------------------------------------------------------
puerto 1   deptA                                         deptA      access    PC
puerto 2   detpB                                         deptB      access    PC
puerto 3   servers                                       servers    access    PC
puerto 4   mkt-internet         escola,isp                          trunk     mkt-puerto1
puerto 5   mkt-xarxes_internes  deptA,deptB,servers                 trunk     mkt-puerto3
puerto 6   roseta               deptA,deptB,servers,isp  escola     hybrid    roseta


Tabla de redes, ips y vlans:

REDES      VLAN      NETWORK-ADDR    IP-ADDR-MKT   DHCP-RANGE
---------------------------------------------------------------
deptA      7xx      10.27.1XX.0/24   .1            .100 - .200
deptB      8xx      10.28.1XX.0/24   .1            .100 - .200
servers    9xx      10.29.1XX.0/24   .1            NO DHCP
escola     1000     192.168.0.0/16   .3.2XX        NO DHCP
isp        1005       10.30.0.0/24   .2XX          NO DHCP
wireless   NO-VLAN  10.50.1XX.0/24   .1            .10 - .250
gestion    NO-VLAN 192.168.88.0/24   .1            NO DHCP


El esquema a nivel de capa 3 es el siguiente.


                    +-----------+             XX XXXXX XXXX
                    |router inf +----------+XXXXXX         XX
                    +-----------+        XX                 X
                          |.1            XX   internet      X  +-------------+
                          |               X              XXXX  |  router ISP |
             -------------+----+-+        XXXXXXXXXXXXXXX+-----+             |
               192.168.0.0/16  |                               +-------------+
                               |                                    |.1
                               |             10.30.0.0/24           |
                               |    +-+-----------------------------++
  +------+                     |      |
  |      |              escola |      | isp
  |server|            vlan1000 |      | vlan1005
  |web   |               port1 |      | port1
  +------+               .3.2XX|      |.2xx
     |.100                  +-------------+
     |                      |             |       +))
     |    10.29.1xx.0/24  .1|             |.1     |wireless
+----+----------------------+   MIKROTIK  +-------+
            vlan9xx    port3|             |    10.50.1xx.0/24
            servers         |             |
                            +--+-----+----+
                            .1 |     |  .1
                       vlan7xx |     |  vlan8xx
                         port3 |     |  port3
                         deptA |     |  deptB
                               |     |
                               |     |
      +------------------------+-+   |
          10.27.1xx.0/24           +-+------------------------------+
                                         10.28.1xx.0/24


# Resolución

Pasos:
1. Configurar switch
2. Configurar interfaces virtuales y direcciones IP en el router
3. Configurar servidores dhcp
4. Definir default gateway por router inf y verificar que salen, cambiar
default gateway por router-isp y verificar que salen. Dejar la ruta con más 
peso a router-inf. Verificar que al desconectar router-inf se cambia la salida por
router-isp
5. Activar las marcas de rutas para que desde la vlan de servers se salga
por router-isp y desde el resto de redes se salga por router-inf
6. Reglas de firewall para evitar accesos desde deptA a deptB y desde
wifi solo a server-web y a internet
7. Reglas de firewall para que sólo haya telnet desde ips de gestión 
y servidor ssh por puerto 12322 
8. Limitar ancho de banda de bajada en wireless a 500Kbps
9. Priorizar tráfico de descarga del deptA respecto a deptB
10. Repartir por igual el tráfico entre todos los usuarios de deptA y deptB
